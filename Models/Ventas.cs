﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class Ventas
    {
        public int IdVenta { get; set; }
        public int IdProducto { get; set; }
        public int Cantidad { get; set; }
        public int Total { get; set; }
    }
}
