﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class Renta
    {
        public int IdRenta { get; set; }
        public int IdProducto { get; set; }
        public decimal Total { get; set; }
        public int IdVendedor { get; set; }
        public int IdCliente { get; set; }
    }
}
