﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Models
{
    public partial class Renta
    {
        public override string ToString()
        {
            return $"{IdRenta} {IdCliente} {IdProducto} {IdVendedor} {Total}";
        }
    }
}
