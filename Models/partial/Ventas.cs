﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Models
{
    public partial class Ventas
    {
        public override string ToString()
        {
            return $"{IdVenta} {IdProducto} {Cantidad} {Total}";
        }
    }
}
