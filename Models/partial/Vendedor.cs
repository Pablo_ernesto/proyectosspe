﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Models
{
    public partial class Vendedor
    {
        public override string ToString()
        {
            return $"{IdVendedor} {NombreV} {ApellidoV} {NumeroCel}";
        }
    }
}
