﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Smart.Models
{
    public partial class Clientes
    {
        public override string ToString()
        {
            return $"{IdCliente} {NombreC} {ApellidoV} {NumeroTelefono}";
        }
    }
}
