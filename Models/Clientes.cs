﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class Clientes
    {
        public int IdCliente { get; set; }
        public string NombreC { get; set; }
        public string ApellidoV { get; set; }
        public string NumeroTelefono { get; set; }
    }
}
