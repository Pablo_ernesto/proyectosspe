﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class Autos
    {
        public int IdProducto { get; set; }
        public string Producto { get; set; }
        public decimal PrecioP { get; set; }
    }
}
