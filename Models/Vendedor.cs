﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class Vendedor
    {
        public int IdVendedor { get; set; }
        public string NombreV { get; set; }
        public string ApellidoV { get; set; }
        public string NumeroCel { get; set; }
    }
}
