﻿using Smart.Models;
using System;
using System.Linq;

namespace Smart
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                int opcion;
                do
                {
                    Console.WriteLine("");
                    Console.WriteLine("SMART-TECH");
                    Console.WriteLine("");
                    Console.WriteLine("1) Nueva renta");
                    Console.WriteLine("2) Buscar renta");
                    Console.WriteLine("3) Mostrar rentas");
                    Console.WriteLine("4) Nuevo >");
                    Console.WriteLine("5) Consultar >");
                    Console.WriteLine("6) Eliminar >");
                    Console.WriteLine("7) Actualizar >");
                    Console.WriteLine("8) Hacer Venta >");
                    Console.WriteLine("0) Salir");

                    Console.Write("Opcion: ");
                    opcion = int.Parse(Console.ReadLine());
                    Console.WriteLine("");

                    switch (opcion)
                    {
                        case 1:
                            MostrarAutos();
                            AgregarRenta();
                            break;
                        case 2:
                            BuscarRenta();
                            break;
                        case 3:
                            MostrarRenta();
                            break;
                        case 4:
                            Console.WriteLine("Nuevo registro");
                            Console.WriteLine("1) Agregar auto");
                            Console.WriteLine("2) Agregar cliente");
                            Console.WriteLine("3) Agregar empleado");
                            Console.WriteLine("0) Volver");
                            Console.Write("Opcion: ");
                            int opcionA;
                            opcionA = int.Parse(Console.ReadLine());
                            switch (opcionA)
                            {
                                case 1:
                                    AgregarAuto();
                                    break;
                                case 2:
                                    AgregarCliente();
                                    break;
                                case 3:
                                    AgregarEmpleado();
                                    break;
                                case 0:
                                    Main(args);
                                    break;
                                default:
                                    Console.WriteLine("Seleccion ivalida");
                                    break;
                            }
                            break;
                        case 5:
                            Console.WriteLine("Consultar");
                            Console.WriteLine("1) Mostrar auto");
                            Console.WriteLine("2) Mostrar Clientes");
                            Console.WriteLine("3) Mostrar empleados");
                            Console.WriteLine("4) Buscar auto");
                            Console.WriteLine("5) Buscar clientes");
                            Console.WriteLine("6) Buscar empleado");
                            Console.WriteLine("0) Volver");
                            Console.Write("Opcion: ");
                            int opcionB;
                            opcionB = int.Parse(Console.ReadLine());
                            switch (opcionB)
                            {
                                case 1:
                                    MostrarAutos();
                                    break;
                                case 2:
                                    MostrarClientes();
                                    break;
                                case 3:
                                    MostrarEmpleado();
                                    break;
                                case 4:
                                    BuscarAuto();
                                    break;
                                case 5:
                                    BuscarCliente();
                                    break;
                                case 6:
                                    BuscarEmpleado();
                                    break;
                                case 0:
                                    Main(args);
                                    break;
                                default:
                                    Console.WriteLine("Seleccion ivalida");
                                    break;
                            }
                            break;
                        case 6:
                            Console.WriteLine("Eliminar");
                            Console.WriteLine("1) Eliminar auto");
                            Console.WriteLine("2) Eliminar Clientes");
                            Console.WriteLine("3) Eliminar renta");
                            Console.WriteLine("4) Eliminar empleado");
                            Console.WriteLine("0) Volver");
                            Console.Write("Opcion: ");
                            int opcionC;
                            opcionC = int.Parse(Console.ReadLine());
                            switch (opcionC)
                            {
                                case 1:
                                    MostrarAutos();
                                    EliminarAuto();
                                    break;
                                case 2:
                                    MostrarClientes();
                                    EliminarCliente();
                                    break;
                                case 3:
                                    MostrarRenta();
                                    EliminarRenta();
                                    break;
                                case 4:
                                    MostrarEmpleado();
                                    EliminarEmpleado();
                                    break;
                                case 0:
                                    Main(args);
                                    break;
                                default:
                                    Console.WriteLine("Seleccion ivalida");
                                    break;
                            }
                            break;
                        case 7:
                            Console.WriteLine("Actualizar");
                            Console.WriteLine("1) Actualizar auto");
                            Console.WriteLine("2) Actualizar Clientes");
                            Console.WriteLine("3) Actualizar renta");
                            Console.WriteLine("4) Actualizar empleado");
                            Console.WriteLine("0) Volver");
                            Console.Write("Opcion: ");
                            int opcionD;
                            opcionD = int.Parse(Console.ReadLine());
                            switch (opcionD)
                            {
                                case 1:
                                    MostrarAutos();
                                    ActualizarAuto();
                                    break;
                                case 2:
                                    MostrarClientes();
                                    ActualizarCliente();
                                    break;
                                case 3:
                                    MostrarRenta();
                                    ActualizarRenta();
                                    break;
                                case 4:
                                    MostrarEmpleado();
                                    ActualizarEmpleado();
                                    break;
                                case 0:
                                    Main(args);
                                    break;
                                default:
                                    Console.WriteLine("Seleccion ivalida");
                                    break;
                            }
                            break;
                        case 8:
                            Console.WriteLine("VENTAS DE AUTOS");
                            Console.WriteLine("1) Agregar");
                            Console.WriteLine("2) Mostrar");
                            Console.WriteLine("3) Eliminar");
                            Console.WriteLine("4) Actualizar");
                            Console.WriteLine("5) Buscar");
                            Console.WriteLine("0) Volver");
                            Console.Write("Opcion: ");
                            int opciond;
                            opciond = int.Parse(Console.ReadLine());
                            switch (opciond)
                            {
                                case 1:
                                    AgregarAuto();
                                    AgregarVenta();
                                    break;
                                case 2:
                                    MostrarVentas();
                                    break;
                                case 3:
                                    MostrarVentas();
                                    EliminarVenta();
                                    break;
                                case 4:
                                    MostrarVentas();
                                    ActualizarVenta();
                                    break;
                                case 5:
                                    BuscarVenta();
                                    break;
                                case 0:
                                    Main(args);
                                    break;
                                default:
                                    Console.WriteLine("Seleccion ivalida");
                                    break;
                            }
                            break;
                        case 0:
                            Console.Write("");
                            break;
                        default:
                            Console.WriteLine("Se ha insertado un numero inválido, intente de nuevo.");
                            break;
                    }
                } while (opcion != 0);

            }
            catch (Exception e)
            {
                Console.WriteLine("No puede dejar vacío el campo o escribir letras/simbolos" + e);
                Main(args);

            }
        }

        public static void AgregarRenta()
        {
            try
            {
                int id;
                int empleo, recibio;
                Console.WriteLine("Nueva renta ");
                Renta rentas = new Renta();
                Vendedor empleado = new Vendedor();
                Clientes clientes = new Clientes();

                Console.Write("Vendedor (Id): ");
                empleo = int.Parse(Console.ReadLine());
                rentas.IdVendedor = empleo;
                try
                {

                    Console.Write("Numero de auto: ");
                    id = rentas.IdProducto = int.Parse(Console.ReadLine());
                    using (SmartContext context = new SmartContext())
                    {
                        foreach (Autos auto in context.Autos)
                        {
                            if (auto.IdProducto == id)
                            {
                                Console.Write($"{auto.IdProducto}) AUTO: {auto.Producto} " +
                                $"| PRECIO: ${auto.PrecioP} | ");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Campos vacíos o Inválidos 1 " + e);
                    AgregarRenta();
                }

                using (SmartContext context = new SmartContext())
                {
                    Console.Write("Cliente (id): ");
                    recibio = int.Parse(Console.ReadLine());
                    rentas.IdCliente = recibio;
                    using (SmartContext context2 = new SmartContext())
                    {
                        IQueryable<Clientes> clientes1 = context2.Clientes.Where(c => c.IdCliente.Equals(recibio));
                        foreach (Clientes cliente in clientes1)
                        {
                            Console.WriteLine($"Cliente: {cliente.NombreC} {cliente.ApellidoV}");
                        }
                    }
                    using (SmartContext context2 = new SmartContext())
                    {
                        IQueryable<Vendedor> empleados = context2.Vendedor.Where(v => v.IdVendedor.Equals(empleo));
                        foreach (Vendedor vendedor in empleados)
                        {
                            Console.WriteLine($"Empleado: {vendedor.NombreV} {vendedor.ApellidoV} | " +
                        $"Código del empleado: {vendedor.IdVendedor}");
                        }
                    }
                    context.Add(rentas);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Campos inválidos o vacíos 2 " + e);
                AgregarRenta();
            }
        }

        public static void EliminarRenta()
        {
            try
            {
                Console.WriteLine("Inserte el id de la renta que desea eliminar.");
                Renta renta = new Renta();

                Console.Write("Numero: ");
                renta.IdRenta = int.Parse(Console.ReadLine());

                using (SmartContext context = new SmartContext())
                {
                    context.Remove(renta);
                    context.SaveChanges();
                    Console.WriteLine("La renta se ha eliminado exitosamente!");
                }
            }
            catch
            {
                Console.WriteLine("Campos vacíos o inválidos");
                EliminarRenta();
            }
        }
        public static void ActualizarRenta()
        {
            try
            {
                int id;
                int empleo, recibio;
                Console.WriteLine("Actualizar renta, inserte el id: ");
                Renta rentas = new Renta();
                Vendedor empleado = new Vendedor();
                Clientes clientes = new Clientes();

                Console.Write("Numero: ");
                id = rentas.IdRenta = int.Parse(Console.ReadLine());

                Console.Write("Vendedor (Id): ");
                empleo = int.Parse(Console.ReadLine());
                rentas.IdVendedor = empleo;
                try
                {

                    Console.Write("Numero de auto: ");
                    id = rentas.IdProducto = int.Parse(Console.ReadLine());
                    using (SmartContext context = new SmartContext())
                    {
                        foreach (Autos auto in context.Autos)
                        {
                            if (auto.IdProducto == id)
                            {
                                Console.Write($"{auto.IdProducto}) AUTO: {auto.Producto} " +
                                $"| PRECIO: ${auto.PrecioP} | ");
                            }
                        }
                    }
                }
                catch
                {
                    Console.WriteLine("Campos vacíos o Inválidos");
                    AgregarRenta();
                }

                using (SmartContext context = new SmartContext())
                {
                    Console.Write("Cliente (id): ");
                    recibio = int.Parse(Console.ReadLine());
                    rentas.IdCliente = recibio;
                    using (SmartContext context2 = new SmartContext())
                    {
                        IQueryable<Clientes> clientes1 = context2.Clientes.Where(c => c.IdCliente.Equals(recibio));
                        foreach (Clientes cliente in clientes1)
                        {
                            Console.WriteLine($"Cliente: {cliente.NombreC} {cliente.ApellidoV}");
                        }
                    }
                    using (SmartContext context2 = new SmartContext())
                    {
                        IQueryable<Vendedor> empleados = context2.Vendedor.Where(v => v.IdVendedor.Equals(empleo));
                        foreach (Vendedor empleado1 in empleados)
                        {
                            Console.WriteLine($"Empleado: {empleado1.NombreV} {empleado1.NombreV} | " +
                        $"Código del entrenador: {empleado1.IdVendedor}");
                        }
                    }
                    context.Add(rentas);
                    context.SaveChanges();
                }
            }
            catch
            {
                Console.WriteLine("Campos inválidos o vacíos");
                ActualizarRenta();
            }
        }
        public static void MostrarRenta()
        {
            Console.WriteLine("Rentas registrados: ");
            using (SmartContext context = new SmartContext())
            {
                foreach (Renta renta in context.Renta)
                {
                    Console.WriteLine($"{renta.IdRenta}) Auto (id): {renta.IdProducto} Precio: {renta.Total}");

                    using (SmartContext context2 = new SmartContext())
                    {
                        IQueryable<Clientes> clientes = context2.Clientes.Where(c => c.IdCliente.Equals(renta.IdCliente));
                        foreach (Clientes cliente in clientes)
                        {
                            Console.WriteLine($"Cliente: {cliente.NombreC} {cliente.ApellidoV} ({cliente.IdCliente})");
                        }
                    }

                    using (SmartContext context2 = new SmartContext())
                    {
                        IQueryable<Vendedor> empleados = context2.Vendedor.Where(v => v.IdVendedor.Equals(renta.IdVendedor));
                        foreach (Vendedor empleado in empleados)
                        {
                            Console.WriteLine($"vendedor: {empleado.NombreV} {empleado.ApellidoV} | " +
                        $"Código del vendedor: {empleado.IdVendedor}");
                        }
                    }
                }
            }
        }
        public static void BuscarRenta()
        {
            Console.WriteLine("Inserte el id de la renta: ");
            int buscar = int.Parse(Console.ReadLine());

            using (SmartContext context = new SmartContext())
            {
                IQueryable<Renta> rentas = context.Renta.Where(v => v.IdRenta.Equals(buscar));
                foreach (Renta renta in rentas)
                {
                    Console.WriteLine($"Consulta realizada con exito: ");
                    Console.WriteLine($"{renta.IdRenta}) Auto: {renta.IdProducto} " +
                        $"| Empleado id: {renta.IdVendedor} " +
                        $"| Cliente: {renta.IdCliente} " +
                        $"| Total: {renta.Total}");
                }
            }
        }

        public static void AgregarAuto()
        {
            try
            {
                Console.WriteLine("Nuevo auto ");
                Autos auto = new Autos();

                Console.Write("AUTO: ");
                auto.Producto = Console.ReadLine();
                Console.Write("Precio: ");
                auto.PrecioP = decimal.Parse(Console.ReadLine());

                using (SmartContext context = new SmartContext())
                {
                    context.Add(auto);
                    context.SaveChanges();
                    Console.WriteLine("El auto se ha resgitrado con éxito!");
                }
            }
            catch
            {
                Console.WriteLine("Campos inválidos o vacíos");
                AgregarAuto();
            }
        }

        public static void MostrarAutos()
        {
            Console.WriteLine("Autos registradas: ");
            using (SmartContext context = new SmartContext())
            {
                foreach (Autos auto in context.Autos)
                {
                    Console.WriteLine($"{auto.IdProducto}) AUTO: {auto.Producto} " +
                        $"| Precio: {auto.PrecioP}");
                }
            }
        }

        public static void EliminarAuto()
        {
            try
            {
                Console.WriteLine("Inserte el numero del auto que desea eliminar.");
                Autos auto = new Autos();

                Console.Write("Numero: ");
                auto.IdProducto = int.Parse(Console.ReadLine());

                using (SmartContext context = new SmartContext())
                {
                    context.Remove(auto);
                    context.SaveChanges();
                    Console.WriteLine("El auto fue eliminado con exito!");
                }
            }
            catch
            {
                Console.WriteLine("Campos inválidos o vacíos");
                EliminarAuto();
            }
        }

        public static void ActualizarAuto()
        {
            try
            {
                Console.WriteLine("Inserte el id del auto que desea actualizar.");
                Autos auto = new Autos();

                Console.Write("Numero: ");
                auto.IdProducto = int.Parse(Console.ReadLine());

                Console.Write("AUTO: ");
                auto.Producto = Console.ReadLine();
                Console.Write("Precio: ");
                auto.PrecioP = decimal.Parse(Console.ReadLine());

                using (SmartContext context = new SmartContext())
                {
                    context.Add(auto);
                    context.SaveChanges();
                    Console.WriteLine("El auto se ha resgitrado con éxito!");
                }
            }
            catch
            {
                Console.WriteLine("Campos inválidos o vacíos");
                ActualizarAuto();
            }
        }

        public static void BuscarAuto()
        {
            Console.WriteLine("Inserte el id de la Auto: ");
            int buscar = int.Parse(Console.ReadLine());

            using (SmartContext context = new SmartContext())
            {
                IQueryable<Autos> autos = context.Autos.Where(p => p.IdProducto.Equals(buscar));
                foreach (Autos auto in autos)
                {
                    Console.WriteLine($"Consulta realizada con exito: ");
                    Console.WriteLine($"{auto.IdProducto}) AUTO: {auto.Producto} " +
                        $"| Precio: {auto.PrecioP}");
                }
            }
        }

        public static void AgregarCliente()
        {
            try
            {
                Console.WriteLine("Nuevo cliente ");
                Clientes cliente = new Clientes();

                Console.Write("Nombre: ");
                cliente.NombreC = Console.ReadLine();
                Console.Write("Apellido: ");
                cliente.ApellidoV = Console.ReadLine();
                Console.Write("Numero de telefono: ");
                cliente.NumeroTelefono = Console.ReadLine();

                using (SmartContext context = new SmartContext())
                {
                    context.Add(cliente);
                    context.SaveChanges();
                    Console.WriteLine("Cliente resgitrado con éxito!");
                }
            }
            catch
            {
                Console.WriteLine("Campos inválidos o vacíos");
                AgregarCliente();
            }
        }

        public static void MostrarClientes()
        {
            Console.WriteLine("Clientes registrados: ");
            using (SmartContext context = new SmartContext())
            {
                foreach (Clientes cliente in context.Clientes)
                {
                    Console.WriteLine($"{cliente.IdCliente}) Nombre: {cliente.NombreC} " +
                        $"{cliente.ApellidoV} " +
                        $"| Telefono: {cliente.NumeroTelefono}");
                }
            }
        }

        public static void EliminarCliente()
        {
            try
            {
                Console.WriteLine("Inserte el id de Cliente que desea eliminar.");
                Clientes clientes = new Clientes();

                Console.Write("Numero: ");
                clientes.IdCliente = int.Parse(Console.ReadLine());

                using (SmartContext context = new SmartContext())
                {
                    context.Remove(clientes);
                    context.SaveChanges();
                    Console.WriteLine("El cliente fue eliminado con exito!");
                }
            }
            catch
            {
                Console.WriteLine("Campos inválidos o vacíos");
                EliminarCliente();
            }
        }

        public static void ActualizarCliente()
        {
            try
            {
                Console.WriteLine("Inserte el id de cliente que desea actualizar.");
                Clientes cliente = new Clientes();

                Console.Write("Numero: ");
                cliente.IdCliente = int.Parse(Console.ReadLine());

                Console.Write("Nombre: ");
                cliente.NombreC = Console.ReadLine();
                Console.Write("Apellido: ");
                cliente.ApellidoV = Console.ReadLine();
                Console.Write("Telefono: ");
                cliente.NumeroTelefono = Console.ReadLine();

                using (SmartContext context = new SmartContext())
                {
                    context.Update(cliente);
                    context.SaveChanges();
                    Console.WriteLine("El cliente fue actualizado con exito!");
                }
            }
            catch
            {
                Console.WriteLine("Campos inválidos o vacíos");
                ActualizarCliente();
            }
        }
        public static void BuscarCliente()
        {
            Console.WriteLine("Inserte el id del cliente: ");
            int buscar = int.Parse(Console.ReadLine());

            using (SmartContext context = new SmartContext())
            {
                IQueryable<Clientes> clientes = context.Clientes.Where(c => c.IdCliente.Equals(buscar));
                foreach (Clientes cliente in clientes)
                {
                    Console.WriteLine($"Consulta realizada con exito: ");
                    Console.WriteLine($"{cliente.IdCliente}) Nombre: {cliente.NombreC} " +
                        $"{cliente.ApellidoV} | Telefono: {cliente.NumeroTelefono}");
                }
            }
        }

        public static void AgregarEmpleado()
        {
            try
            {
                Console.WriteLine("Nuevo Empleado");
                Vendedor empleado = new Vendedor();

                Console.Write("Nombre: ");
                empleado.NombreV = Console.ReadLine();
                Console.Write("Apellido: ");
                empleado.ApellidoV = Console.ReadLine();
                Console.Write("Numero de telefono: ");
                empleado.NumeroCel = Console.ReadLine();

                using (SmartContext context = new SmartContext())
                {
                    context.Add(empleado);
                    context.SaveChanges();
                    Console.WriteLine("Empleado resgitrado con éxito!");
                }
            }
            catch
            {
                Console.WriteLine("Campos inválidos o vacíos");
                AgregarEmpleado();
            }
        }

        public static void MostrarEmpleado()
        {
            Console.WriteLine("Empleado registrados: ");
            using (SmartContext context = new SmartContext())
            {
                foreach (Vendedor empleado in context.Vendedor)
                {
                    Console.WriteLine($"{empleado.IdVendedor}) " +
                        $"Nombre: {empleado.NombreV} {empleado.ApellidoV} " +
                        $"| Numero: {empleado.NumeroCel}");
                }
            }
        }

        public static void EliminarEmpleado()
        {
            try
            {
                Console.WriteLine("Inserte el id del Empleado que desea eliminar.");
                Vendedor empleado = new Vendedor();

                Console.Write("Numero: ");
                empleado.IdVendedor = int.Parse(Console.ReadLine());

                using (SmartContext context = new SmartContext())
                {
                    context.Remove(empleado);
                    context.SaveChanges();
                    Console.WriteLine("El empleado fue eliminado con exito!");
                }
            }
            catch
            {
                Console.WriteLine("Campos inválidos o vacíos");
                EliminarEmpleado();
            }
        }

        public static void ActualizarEmpleado()
        {
            try
            {
                Console.WriteLine("Inserte el id del empleado que desea actualizar.");
                Vendedor empleado = new Vendedor();

                Console.Write("Numero: ");
                empleado.IdVendedor = int.Parse(Console.ReadLine());

                Console.Write("Nombre: ");
                empleado.NombreV = Console.ReadLine();
                Console.Write("Apellido: ");
                empleado.ApellidoV = Console.ReadLine();
                Console.Write("Numerpo de telefono: ");
                empleado.NumeroCel = Console.ReadLine();

                using (SmartContext context = new SmartContext())
                {
                    context.Add(empleado);
                    context.SaveChanges();
                    Console.WriteLine("Empleado resgitrado con éxito!");
                }
            }
            catch
            {
                Console.WriteLine("Campos inválidos o vacíos");
                ActualizarEmpleado();
            }
        }
        public static void BuscarEmpleado()
        {
            Console.WriteLine("Inserte el id del empleado: ");
            int buscar = int.Parse(Console.ReadLine());

            using (SmartContext context = new SmartContext())
            {
                IQueryable<Vendedor> empleados = context.Vendedor.Where(v => v.IdVendedor.Equals(buscar));
                foreach (Vendedor empleado in empleados)
                {
                    Console.WriteLine($"Consulta realizada con exito: ");
                    Console.WriteLine($"{empleado.IdVendedor}) Empleado: {empleado.NombreV} " +
                        $"{empleado.ApellidoV} | Telefono: {empleado.NumeroCel}");
                }
            }
        }
        public static void AgregarVenta()
        {
            try
            {
                int id;
                Console.WriteLine("Nueva venta ");
                Ventas ventas = new Ventas();
                Vendedor empleado = new Vendedor();
                Clientes clientes = new Clientes();

                try
                {

                    Console.Write("Numero de AUTO: ");
                    id = ventas.IdProducto = int.Parse(Console.ReadLine());
                    using (SmartContext context = new SmartContext())
                    {
                        foreach (Autos auto in context.Autos)
                        {
                            if (auto.IdProducto == id)
                            {
                                Console.Write($"{auto.IdProducto}) AUTO: {auto.Producto} " +
                                $"| PRECIO: ${auto.PrecioP} | ");
                            }
                        }
                    }
                }
                catch
                {
                    Console.WriteLine("Campos vacíos o Inválidos");
                    AgregarVenta();
                }

                using (SmartContext context = new SmartContext())
                {
                    context.Add(ventas);
                    context.SaveChanges();
                }
            }
            catch
            {
                Console.WriteLine("Campos inválidos o vacíos");
                AgregarVenta();
            }
        }
        public static void EliminarVenta()
        {
            try
            {
                Console.WriteLine("Inserte el id de la venta que desea eliminar.");
                Ventas venta = new Ventas();

                Console.Write("Numero: ");
                venta.IdVenta = int.Parse(Console.ReadLine());

                using (SmartContext context = new SmartContext())
                {
                    context.Remove(venta);
                    context.SaveChanges();
                    Console.WriteLine("La Venta se ha eliminado exitosamente!");
                }
            }
            catch
            {
                Console.WriteLine("Campos vacíos o inválidos");
                EliminarRenta();
            }
        }

        public static void ActualizarVenta()
        {
            try
            {
                int id;
                Console.WriteLine("Inserte el id del maneger a actualizar.");
                Ventas ventas = new Ventas();
                Autos autos = new Autos();

                Console.Write("Numero: ");
                ventas.IdVenta = int.Parse(Console.ReadLine());
                try
                {

                    Console.Write("Numero de pelicula: ");
                    id = ventas.IdProducto = int.Parse(Console.ReadLine());
                    using (SmartContext context = new SmartContext())
                    {
                        foreach (Autos auto in context.Autos)
                        {
                            if (auto.IdProducto == id)
                            {
                                Console.Write($"{auto.IdProducto}) AUTO: {auto.Producto} " +
                                $"| PRECIO: ${auto.PrecioP} | ");
                            }
                        }
                    }
                }
                catch
                {
                    Console.WriteLine("Campos vacíos o Inválidos");
                    AgregarVenta();
                }

                using (SmartContext context = new SmartContext())
                {
                    context.Update(ventas);
                    context.SaveChanges();
                    Console.WriteLine("LA venta fue actualizado con exito!");
                }
            }
            catch
            {
                Console.WriteLine("Campos inválidos o vacíos");
                ActualizarVenta();
            }
        }

        public static void BuscarVenta()
        {
            Console.WriteLine("Inserte el id de la venta: ");
            int buscar = int.Parse(Console.ReadLine());

            using (SmartContext context = new SmartContext())
            {
                IQueryable<Ventas> ventas = context.Ventas.Where(vm => vm.IdVenta.Equals(buscar));
                foreach (Ventas venta in ventas)
                {
                    Console.WriteLine($"Consulta realizada con exito: ");
                    Console.WriteLine($"{venta.IdVenta}) AUTO: {venta.IdProducto} " +
                        $"| PRecio: {venta.Total} ");
                }
            }
        }
        public static void MostrarVentas()
        {
            Console.WriteLine("Ventas: ");
            using (SmartContext context = new SmartContext())
            {
                foreach (Ventas ventas in context.Ventas)
                {
                    Console.WriteLine($"{ventas.IdVenta}) AUTO: {ventas.IdProducto} " +
                         $"| Precio: {ventas.Total}");
                }
            }
        }
    }
}